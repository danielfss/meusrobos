package MeusRobos;

import java.awt.Color;

import robocode.*;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/*
 * Teste da Solutis - Robocode
 * Participante: Daniel Ferreira Santos Souza.
 */

public class MeuRobo1 extends Robot
{

	boolean angulo = true;
	
	public void run() {

		setBodyColor(Color.yellow); // cor do corpo
		setGunColor(Color.white); // cor do canhao
		setRadarColor(Color.black); // cor do radar
		setScanColor(Color.yellow); // cor do escaneamento
		setBulletColor(Color.white); // cor da bala
		
		
		// Robot main loop
		while(true) {
			if (angulo == true) {
				// LA�O EM 90 GRAUS
				for(int voltasDir90 = 0; voltasDir90 < 4; voltasDir90++) {
					ahead(200);
					turnRight(90);
					turnGunLeft(360);
				}
				
				for(int voltasEsq90 = 4; voltasEsq90 > 0; voltasEsq90--) {
					ahead(200);
					turnLeft(90);
					turnGunRight(360);
				}
				angulo = false;
			} else {
				// LA�O EM 45 GRAUS
				for(int voltasDir45 = 0; voltasDir45 < 8; voltasDir45++) {
					ahead(100);
					turnRight(45);
					turnGunLeft(360);
				}
				
				for(int voltasEsq45 = 8; voltasEsq45 > 0; voltasEsq45--) {
					ahead(100);
					turnLeft(45);
					turnGunRight(360);
				}
				angulo = true;
			} // fim if-else
		}
	}

	
	// Quando avistar um inimigo
	public void onScannedRobot(ScannedRobotEvent e) {
		fire(2);
	}

	
	// Se receber um tiro, recue 100 pixels
	public void onHitByBullet(HitByBulletEvent e) {
		turnLeft(90 - e.getBearing());
	}
	
	// Se trombar com um rob� inimigo, vire 45 graus a direita
	public void onHitRobot(HitRobotEvent e) {
		if (e.isMyFault()) {
			turnRight(45);
		}
	}
	
	// Se trombar com a parede, vire 90 graus a direita
	public void onHitWall(HitWallEvent e) {
			turnRight(90);
			back(200);
	}	
}
