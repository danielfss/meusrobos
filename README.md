Esse projeto foi construído para participar do desafio da Solutis - Talent Sprint.
Robocode é um jogo de programação, onde o objetivo é desenvolver um robô, no estilo tanque de guerra.
Tenho pouca experiência em Java, mas pesquisando bastante e usurfruindo da documentação e API da Robocode, tornou o desenvolvimento bem divertido.

Inicialmente, foi feita a definição das cores do robô. Essa parte de personalização pode ser divida em corpo, canhão, radar, varredura e bala.
Logo após a personalização, entramos em um loop infinito. Coloquei uma estrutura de decisão para que entrasse em 2 processos diferentes de repetição.

Logo abaixo, existem alguns métodos da Classe Robot.
O "onScannedRobot", serve para fazer a varredura e encontrar robôs inimigos, após ele escanear algum inimigo, será executada alguma ação. Eu defini para ele atirar quando encontrasse um robô inimigo.

Em seguida vem o "onHitRobot". É quando o robô colide com outro robô. Utilizei a opção isMyFault(), ele detecta quando o meu robô vai se chocar com o outro.

O "onHitByBullet" serve para detectar se existe uma bala indo de encontro com o meu robô. Usando o getBearing(), ele se baseia no angulo da posição do robô e executa um desvio para esquerda usando o método turnLeft();

Por último é o evento onHitWall, o robô detecta que irá se colidir com a parede e executa alguma ação. Inseri apenas dois métodos: turnRight() e back(). Respectivamente, virar à direita e dar a volta.